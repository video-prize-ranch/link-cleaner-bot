package commands

import (
	"log"
	"net/http"
	"regexp"

	"github.com/dyatlov/go-opengraph/opengraph"
	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/event"
)

func Tenor(source mautrix.EventSource, evt *event.Event, client *mautrix.Client) {
	linkRe := regexp.MustCompile(`(http|https):\/\/tenor\.com\/(.*)`)
	link := linkRe.FindString(evt.Content.AsMessage().Body)

	pageRes, err := http.Get(link)
	if err != nil {
		handleErr(err, client, evt)
	}

	og := opengraph.NewOpenGraph()
	err1 := og.ProcessHTML(pageRes.Body)
	if err1 != nil {
		handleErr(err1, client, evt)
	}

	imgRes, err := http.Get(og.Images[0].URL)
	if err != nil {
		handleErr(err, client, evt)
	}

	mxcUrl, err2 := client.UploadMedia(mautrix.ReqUploadMedia{
		Content:       imgRes.Body,
		ContentLength: imgRes.ContentLength,
		ContentType:   "image/gif",
		FileName:      "tenor.gif",
	})
	if err2 != nil {
		handleErr(err2, client, evt)
	}

	_, err4 := client.SendMessageEvent(evt.RoomID, event.EventMessage, &event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "Here is the Tenor GIF.",
		Format:        "org.matrix.custom.html",
		FormattedBody: "Here is the Tenor GIF.",
	})
	if err4 != nil {
		handleErr(err4, client, evt)
	}
	_, err3 := client.SendImage(evt.RoomID, "Here is the Tenor GIF.", mxcUrl.ContentURI)
	if err3 != nil {
		handleErr(err3, client, evt)
	}
}

func handleErr(err error, client *mautrix.Client, evt *event.Event) {
	if err != nil {
		_, err = client.SendMessageEvent(evt.RoomID, event.EventMessage, &event.MessageEventContent{
			MsgType:       "m.text",
			Body:          "I ran into an error processing the Tenor GIF. Alerting @imabritishcow:bcow.xyz",
			Format:        "org.matrix.custom.html",
			FormattedBody: "I ran into an error processing the Tenor GIF. Alerting @imabritishcow:bcow.xyz",
		})
		log.Fatal(err)
	}
}
