package commands

import (
	"log"

	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/event"
)

func LinkCleaner(source mautrix.EventSource, evt *event.Event, client *mautrix.Client) {
	_, err := client.SendMessageEvent(evt.RoomID, event.EventMessage, &event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "I clean links with trackers and redirect to private frontends. I'm made by @imabritishcow:bcow.xyz and my source code is available here: https://github.com/imabritishcow/link-cleaner-bot",
		Format:        "org.matrix.custom.html",
		FormattedBody: `I clean links with trackers and redirect to private frontends. I'm made by <code>@imabritishcow:bcow.xyz</code> and my source code is available on <a href="https://github.com/imabritishcow/link-cleaner-bot">GitHub</a>.`,
	})
	if err != nil {
		log.Fatal(err)
	}
}
