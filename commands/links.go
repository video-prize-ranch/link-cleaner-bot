package commands

import (
	"log"
	"math/rand"
	"regexp"
	"strings"

	"github.com/spf13/viper"
	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/event"
)

func Links(source mautrix.EventSource, evt *event.Event, client *mautrix.Client) {
	linkRe := regexp.MustCompile(`((.*):\/\/)?(.*).(.*)\/([^ ]*)`)
	links := linkRe.FindAllString(evt.Content.AsMessage().Body, 1000)
	newLinks := make([]string, 0)

	for i := 0; i < len(links); i++ {
		link := links[i]

		switch true {
		case strings.Contains(link, "youtube.com"):
			youtubeRe := regexp.MustCompile(`(www\.)?youtube\.com`)
			invidiousInstance := viper.GetStringSlice("INVIDIOUS_INSTANCES")[rand.Intn(len(viper.GetStringSlice("INVIDIOUS_INSTANCES")))]

			newLinks = append(newLinks, youtubeRe.ReplaceAllString(link, invidiousInstance))
		case strings.Contains(link, "youtu.be"):
			youtubeRe2 := regexp.MustCompile(`(www\.)?youtu\.be`)
			invidiousInstance := viper.GetStringSlice("INVIDIOUS_INSTANCES")[rand.Intn(len(viper.GetStringSlice("INVIDIOUS_INSTANCES")))]

			newLinks = append(newLinks, youtubeRe2.ReplaceAllString(link, invidiousInstance))
		case strings.Contains(link, "twitter.com"):
			twitterRe := regexp.MustCompile(`(www\.)?twitter\.com`)
			nitterInstance := viper.GetStringSlice("NITTER_INSTANCES")[rand.Intn(len(viper.GetStringSlice("NITTER_INSTANCES")))]

			newLinks = append(newLinks, twitterRe.ReplaceAllString(link, nitterInstance))
		case strings.Contains(link, "reddit.com"):
			redditRe := regexp.MustCompile(`(www\.)?reddit\.com`)
			redditInstance := viper.GetStringSlice("REDDIT_INSTANCES")[rand.Intn(len(viper.GetStringSlice("REDDIT_INSTANCES")))]

			newLinks = append(newLinks, redditRe.ReplaceAllString(link, redditInstance))
			/*case strings.Contains(link, "odysee.com"):
			re := regexp.MustCompile(`(www\.)?odysee\.com`)
			librarianInstance := viper.GetStringSlice("LIBRARIAN_INSTANCES")[rand.Intn(len(viper.GetStringSlice("LIBRARIAN_INSTANCES")))]

			newLinks = append(newLinks, re.ReplaceAllString(link, librarianInstance))*/
		}
	}

	newLinksStrPlain := ""
	newLinksStrFmt := ""
	for i := 0; i < len(newLinks); i++ {
		newLink := newLinks[i]

		newLinksStrPlain = newLinksStrPlain + newLink + "\n"
		newLinksStrFmt = newLinksStrFmt + newLink + "<br>"
	}

	_, err := client.SendMessageEvent(evt.RoomID, event.EventMessage, &event.MessageEventContent{
		MsgType:       "m.text",
		Body:          "Broken instance? Message `@imabritishcow:bcow.xyz` \nHere are the link(s) in this message on private frontends:\n" + newLinksStrPlain,
		Format:        "org.matrix.custom.html",
		FormattedBody: "<i>Broken instance? Message <code>@imabritishcow:bcow.xyz</code></i> <br>Here are the link(s) in this message on private frontends:<br>" + newLinksStrFmt,
	})
	if err != nil {
		log.Fatal(err)
	}
}
