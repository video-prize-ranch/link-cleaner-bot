module github.com/imabritishcow/link-cleaner-bot

go 1.16

require (
	github.com/dyatlov/go-opengraph v0.0.0-20210112100619-dae8665a5b09
	github.com/spf13/viper v1.7.1
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110 // indirect
	maunium.net/go/mautrix v0.9.1
)
