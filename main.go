package main

import (
	"fmt"
	"log"
	"math/rand"
	"regexp"
	"time"

	"github.com/imabritishcow/link-cleaner-bot/commands"
	"github.com/spf13/viper"
	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/event"
)

func main() {
	botStartTime := time.Now().UTC().Unix() * 1000
	rand.Seed(time.Now().Unix())

	viper.SetConfigName("config")
	viper.SetConfigType("yml")
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	client, err := mautrix.NewClient(viper.GetString("HOMESERVER"), "", "")
	if err != nil {
		log.Fatal(err)
	}

	_, err = client.Login(&mautrix.ReqLogin{
		Type:             "m.login.password",
		Identifier:       mautrix.UserIdentifier{Type: mautrix.IdentifierTypeUser, User: viper.GetString("USERNAME")},
		Password:         viper.GetString("PASSWORD"),
		StoreCredentials: true,
	})
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Successful login")

	syncer := client.Syncer.(*mautrix.DefaultSyncer)

	syncer.OnEventType(event.EventMessage, func(source mautrix.EventSource, evt *event.Event) {
		if (evt.Timestamp > botStartTime) && evt.Sender != client.UserID {
			frontendRe := regexp.MustCompile(`youtube.com|twitter.com|reddit.com|youtu.be`)
			tenorRe := regexp.MustCompile(`(http|https):\/\/tenor\.com\/(.*)`)

			switch true {
			case evt.Content.AsMessage().Body == "!link-cleaner":
				commands.LinkCleaner(source, evt, client)
			case frontendRe.MatchString(evt.Content.AsMessage().Body):
				commands.Links(source, evt, client)
			case tenorRe.MatchString(evt.Content.AsMessage().Body):
				commands.Tenor(source, evt, client)
			}
		}
	})

	syncer.OnEventType(event.StateMember, func(source mautrix.EventSource, evt *event.Event) {
		if source.String() == "invited state" {
			if evt.Content.Raw["displayname"] == viper.GetString("USERNAME") {
				_, err := client.JoinRoomByID(evt.RoomID)

				if err != nil {
					log.Fatal(err)
				}
			}
		}
	})

	err = client.Sync()
	if err != nil {
		log.Fatal(err)
	}
}
